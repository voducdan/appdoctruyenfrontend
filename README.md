# AppDocTruyenFrontend



## Setup local
Install expo-cli global
```
    npm install -g expo-cli
```
Move to root project directory then run:
```
    npm install
```
Start project
```
    npm start
```

# Some materialize

* Materialize design icon: https://materialdesignicons.com/
* React native paper library: https://callstack.github.io/react-native-paper/
