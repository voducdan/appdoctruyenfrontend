import * as React from 'react';
import { BottomNavigation, Text } from 'react-native-paper';
import HomeScreen from '../screens/homeScreen';
import CategoryScreen from '../screens/categoryScreen';

const CollectionRoute = () => <Text>Collection screen</Text>;

const AccountRoute = () => <Text>Account screen</Text>;

const BottomNavigationBar = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'home', title: 'Home', icon: 'home' },
    { key: 'category', title: 'Category', icon: 'view-list' },
    { key: 'collection', title: 'Collection', icon: 'folder' },
    { key: 'account', title: 'Account', icon: 'account' },
  ]);

  const renderScene = BottomNavigation.SceneMap({
    home: HomeScreen,
    category: CategoryScreen,
    collection: CollectionRoute,
    account: AccountRoute,
  });


  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
      barStyle={{ backgroundColor: '#694fad' }}
      style={{
        marginTop: 35
      }}
    />
  );
};



export default BottomNavigationBar;