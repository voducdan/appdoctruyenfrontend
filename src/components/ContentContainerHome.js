import * as React from 'react';
import { Component } from 'react';
import { Image, Text, View, StyleSheet, ScrollView } from 'react-native';
import HomeImage from '../../assets/top10.jpeg';
import PropTypes from 'prop-types';
// import HomeImage1 from '../../assets/truyen-kiem-hiep-4.jpg'
// import HomeImage2 from '../../assets/5d84db1ff2dbf6d6cf91bd2d52ca5b6d.jpg'

export default function ContentContainerHome (props){
      return(
        <View>
           
            <View style={styles.container}> 
                <Image style={styles.topTenImage} source={props.image}/>
                <Text style={styles.title}>{props.title}</Text>
            </View>
          
        </View>
      );
    
  } 
  
    

const styles =StyleSheet.create({
    container:{
        alignItems:'center',
        padding:16,
        borderRadius:25,
        shadowColor:'#000',
        shadowOpacity:0.3,
        shadowRadius:10,
        marginBottom:16,
        shadowOffset:{width:0,height:0}
    },
    topTenImage:{
        width:342,
        height:146,
        borderRadius:25,
      
    },
    title:{
        textTransform:'uppercase',
        marginBottom:8,
        fontWeight:'700'
      

    }
})
ContentContainerHome.propTypes = {
    title: PropTypes.string
  
  }
ContentContainerHome.defaultProps = {
    title: 'New Content',
    image: HomeImage
   
  }

