import * as React from 'react';
import { Image, Text, View, StyleSheet, ScrollView } from 'react-native';
import HomeImage from '../../assets/top10.jpeg';
import HomeImage1 from '../../assets/truyen-kiem-hiep-4.jpg'
import HomeImage2 from '../../assets/5d84db1ff2dbf6d6cf91bd2d52ca5b6d.jpg'
import ContentContainerHome from '../components/ContentContainerHome'

const HomeScreen = () => {
    return(
        <View>
            <ScrollView>
            <ContentContainerHome title={'Top 10 truyện nên đọc'} image={HomeImage}/>
            <ContentContainerHome title={'Truyện mới cập nhật'}image={HomeImage1}/>
            <ContentContainerHome title={'Có thể bạn muốn đọc'}image={HomeImage2}/>
            <ContentContainerHome />
            </ScrollView>
        </View>
    
    )
    };

const styles =StyleSheet.create({
    container:{
        alignItems:'center',
        padding:16,
        borderRadius:25,
        shadowColor:'#000',
        shadowOpacity:0.3,
        shadowRadius:10,
        marginBottom:16,
        shadowOffset:{width:0,height:0}
    },
    topTenImage:{
        width:342,
        height:146,
        borderRadius:25,
      
    },
    title:{
        textTransform:'uppercase',
        marginBottom:8,
        fontWeight:'700'
      

    }
})

export default HomeScreen;